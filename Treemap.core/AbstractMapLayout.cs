﻿using System;

namespace Treemap.Core
{
    public abstract class AbstractMapLayout : IMapLayout
    {
        public enum RectangleDivision
        {
            Vertical,
            Horizontal
        }

        public enum SortOrder
        {
            Ascending,
            Descending
        }

        /** Subclasses implement these methods themselves. */
        public abstract void Layout(IMappable[] items, Rect bounds);

        public abstract string GetName();
        public abstract string GetDescription();

        public void Layout(IMapModel model, Rect bounds)
        {
            Layout(model.GetItems(), bounds);
        }


        public static double TotalSize(IMappable[] items)
        {
            return TotalSize(items, 0, items.Length - 1);
        }

        public static double TotalSize(IMappable[] items, int start, int end)
        {
            //Console.WriteLine($"{start}-{end} ({end-start}) of {items.Length}");
            double sum = 0;
            for (var i = start; i <= end; i++)
                sum += items[i].Size;
            return sum;
        }

        // For a production system, use a quicksort...
        public IMappable[] SortDescending(IMappable[] items)
        {
            var s = new IMappable[items.Length];
            Array.Copy(items, s, s.Length);
            var n = s.Length;
            var outOfOrder = true;
            while (outOfOrder)
            {
                outOfOrder = false;
                for (var i = 0; i < n - 1; i++)
                {
                    var wrong = (s[i].Size < s[i + 1].Size);
                    if (wrong)
                    {
                        IMappable temp = s[i];
                        s[i] = s[i + 1];
                        s[i + 1] = temp;
                        outOfOrder = true;
                    }
                }
            }

            return s;
        }

        public static void SliceLayout(IMappable[] items, int start, int end, Rect bounds,
            RectangleDivision orientation, SortOrder order = SortOrder.Ascending)
        {
            var total = TotalSize(items, start, end);
            double a = 0;
            var vertical = orientation == RectangleDivision.Vertical;

            for (var i = start; i <= end; i++)
            {
                var r = new Rect();
                var b = items[i].Size / total;
                if (vertical)
                {
                    r.x = bounds.x;
                    r.w = bounds.w;
                    if (order == SortOrder.Ascending)
                        r.y = bounds.y + bounds.h * a;
                    else
                        r.y = bounds.y + bounds.h * (1 - a - b);
                    r.h = bounds.h * b;
                }
                else
                {
                    if (order == SortOrder.Ascending)
                        r.x = bounds.x + bounds.w * a;
                    else
                        r.x = bounds.x + bounds.w * (1 - a - b);
                    r.w = bounds.w * b;
                    r.y = bounds.y;
                    r.h = bounds.h;
                }

                items[i].Bounds = r;
                a += b;
            }
        }
    }
}