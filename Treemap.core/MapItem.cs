﻿namespace Treemap.Core
{
    /// <summary>
    /// A simple implementation of the Mappable interface.
    /// </summary>
    public class MapItem : IMappable
    {
        public double Size { get; set; }
        public Rect Bounds { get; set; }
        public int Order { get; set; }
        public int Depth { get; set; }

        public IMappable CopyTo()
        {
            return new MapItem(Size, Order)
            {
                Depth = Depth,
                Bounds = Bounds.Copy()
            };
        }

        public MapItem()
        {
            SetupMapItem(1, 0);
        }

        public MapItem(double size, int order)
        {
            SetupMapItem(size, order);
        }

        private void SetupMapItem(double size, int order)
        {
            Size = size;
            Order = order;
            Bounds = new Rect();
        }

        public override string ToString()
        {
            return $"[{Depth}/{Order}] {Size} at {Bounds}";
        }
    }
}