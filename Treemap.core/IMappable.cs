﻿namespace Treemap.Core
{
    public interface IMappable
    {
        double Size { get; set; }
        Rect Bounds { get; set; }
        int Order { get; set; }
        int Depth { get; set; }
        
        IMappable CopyTo();
    }
}