﻿namespace Treemap.Core
{
    /// <summary>
    /// The original slice-and-dice layout for treemaps.
    /// </summary>
    public class SliceLayout : AbstractMapLayout
    {
        public enum OrientationHint
        {
            Best,
            Alternate,
            Auto
        }
        private readonly OrientationHint _orientation;

        public SliceLayout()
        {
            _orientation = OrientationHint.Alternate;
        }

        public SliceLayout(OrientationHint orientation)
        {
            this._orientation = orientation;
        }

        public override void Layout(IMappable[] items, Rect bounds)
        {
            if (items.Length == 0) return;
            var o = _orientation;
            switch (o)
            {
                case OrientationHint.Best:
                    LayoutBest(items, 0, items.Length - 1, bounds);
                    break;
                case OrientationHint.Alternate:
                    Layout(items, bounds, items[0].Depth % 2 == 0 ? RectangleDivision.Vertical : RectangleDivision.Horizontal);
                    break;
                default:
                    Layout(items, bounds, o == 0 ? RectangleDivision.Vertical : RectangleDivision.Horizontal);
                    break;
            }
        }

        public static void LayoutBest(IMappable[] items, int start, int end, Rect bounds)
        {
            SliceLayout(items, start, end, bounds,
                bounds.w > bounds.h ? RectangleDivision.Horizontal : RectangleDivision.Vertical);
        }

        public static void LayoutBest(IMappable[] items, int start, int end, Rect bounds, SortOrder order)
        {
            SliceLayout(items, start, end, bounds,
                bounds.w > bounds.h ? RectangleDivision.Horizontal : RectangleDivision.Vertical, order);
        }

        public static void Layout(IMappable[] items, Rect bounds, RectangleDivision orientation)
        {
            SliceLayout(items, 0, items.Length - 1, bounds, orientation);
        }

        public override string GetName()
        {
            return "Slice-and-dice";
        }

        public override string GetDescription()
        {
            return "This is the original treemap algorithm, " +
                   "which has excellent stability properies " +
                   "but leads to high aspect ratios.";
        }
    }
}