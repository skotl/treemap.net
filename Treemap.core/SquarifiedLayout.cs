﻿namespace Treemap.Core
{
    /// <summary>
    /// "Squarified" treemap layout invented by J.J. van Wijk.
    /// </summary>
    public class SquarifiedLayout : AbstractMapLayout
    {
        public override void Layout(IMappable[] items, Rect bounds)
        {
            Layout(SortDescending(items),0,items.Length-1,bounds);
        }
        
        public void Layout(IMappable[] items, int start, int end, Rect bounds)
        {
            if (start>end) return;
            
            if (end-start<2)
            {
                Core.SliceLayout.LayoutBest(items,start,end,bounds);
                return;
            }
        
            double x=bounds.x, y=bounds.y, w=bounds.w, h=bounds.h;
        
            double total=Sum(items, start, end);
            var mid=start;
            var a=items[start].Size/total;
            var b=a;
        
            if (w<h)
            {
                // height/width
                while (mid<=end)
                {
                    var aspect=NormAspect(h,w,a,b);
                    var q=items[mid].Size/total;
                    if (NormAspect(h,w,a,b+q)>aspect) break;
                    mid++;
                    b+=q;
                }
                Core.SliceLayout.LayoutBest(items,start,mid,new Rect(x,y,w,h*b));
                Layout(items,mid+1,end,new Rect(x,y+h*b,w,h*(1-b)));
            }
            else
            {
                // width/height
                while (mid<=end)
                {
                    var aspect=NormAspect(w,h,a,b);
                    var q=items[mid].Size/total;
                    if (NormAspect(w,h,a,b+q)>aspect) break;
                    mid++;
                    b+=q;
                }
                Core.SliceLayout.LayoutBest(items,start,mid,new Rect(x,y,w*b,h));
                Layout(items,mid+1,end,new Rect(x+w*b,y,w*(1-b),h));
            }
        }
        
        private double Aspect(double big, double small, double a, double b)
        {
            return (big*b)/(small*a/b);
        }
        
        private double NormAspect(double big, double small, double a, double b)
        {
            var x=Aspect(big,small,a,b);
            if (x<1) return 1/x;
            return x;
        }
        
        private double Sum(IMappable[] items, int start, int end)
        {
            var s=0d;
            for (var i=start; i<=end; i++)
                s+=items[i].Size;
            return s;
        }
        
        public override string GetName()
        {
            return "Squarified";
        }
    
        public override string GetDescription()
        {
            return "Algorithm used by J.J. van Wijk.";
        }
    }
}