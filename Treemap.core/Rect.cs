﻿using System;

namespace Treemap.Core
{
    public class Rect
    {
        public double x { get; set; }
        public double y { get; set; }
        public double w { get; set; }
        public double h { get; set; }
        
        public Rect()
        {
            SetRect(0, 0, 1, 1);    
        }
        
        public Rect(Rect r)
        {
            SetRect(r.x, r.y, r.w, r.h);
        }

        public Rect(double x, double y, double w, double h)
        {
            SetRect(x, y, w, h);
        }

        public void SetRect(double x, double y, double w, double h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }
    
        public double AspectRatio()
        {
            return Math.Max(w/h, h/w);
        }
    
        public double Distance(Rect r)
        {
            return Math.Sqrt((r.x-x)*(r.x-x)+
                             (r.y-y)*(r.y-y)+
                             (r.w-w)*(r.w-w)+
                             (r.h-h)*(r.h-h));
        }
    
        public Rect Copy()
        {
            return new Rect(x,y,w,h);
        }

        public override string ToString()
        {
            return $"xy=({x}, {y}), size=({w}, {h})";
        }

    }
}