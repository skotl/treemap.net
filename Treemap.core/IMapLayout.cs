﻿namespace Treemap.Core
{
    /// The interface for all treemap layout algorithms.
    /// If you write your own algorith, it should conform
    /// to this interface.
    /// <p>IMPORTANT: if you want to be able to automatically plug 
    /// your algorithm into the various demos and test harnesses
    /// included in the treemap package, it should have
    /// an empty constructor.</p>
    public interface IMapLayout
    {
        /// <summary>
        /// Arrange the items in the given MapModel to fill the given rectangle.
        /// </summary>
        /// <param name="model">The MapModel</param>
        /// <param name="bounds">The boundsing rectangle for the layout.</param>
        void Layout(IMapModel model, Rect bounds);
        
        /// <summary>
        /// Return a human-readable name for this layout; used to label figures, tables, etc.
        /// </summary>
        /// <returns>String naming this layout</returns>
        string GetName();
        
        /// <summary>
        /// Return a longer description of this layout;
        /// Helpful in creating online-help, interactive catalogs or indices to lists of algorithms.
        /// </summary>
        /// <returns>String describing this layout</returns>
        string GetDescription();
    }
}