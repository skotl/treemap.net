﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Treemap.Core
{
    /// <summary>
    /// An implementation of MapModel that represents a hierarchical structure. It currently cannot
    /// handle structural changes to the tree, since it caches a fair amount of information.
    /// </summary>
    public class TreeModel : IMapModel
    {
        private IMappable[] childItems;
        private IMappable[] cachedTreeItems; // we assume tree structure doesn't change.
        private IMapModel[] cachedLeafModels;
        private TreeModel parent;
        private List<IMapModel> children = new List<IMapModel>();
        private bool sumsChildren;

        public int ChildCount => children.Count;

        public bool HasChildren => children.Count > 0;
        
        public IMappable MapItem { get; private set; }

        public int Depth => 1 + parent?.Depth ?? 0;
        
        public TreeModel Parent
        {
            get => parent;
            private set
            {
                for (var p = value; p != null; p = p.Parent)
                    if (p == this)
                        throw new ArgumentOutOfRangeException("Circular ancestry!");
                parent = value;
            }
        }
        
        public TreeModel()
        {
            MapItem = new MapItem();
            sumsChildren = true;
        }

        public TreeModel(IMappable mapItem)
        {
            MapItem = mapItem;
        }

        public IMapModel CopyTo()
        {
            return new TreeModel(MapItem)
            {
                childItems = CopyArray(childItems),
                cachedLeafModels = CopyArray(cachedLeafModels),
                cachedTreeItems = CopyArray(cachedTreeItems),
                children = CopyArray(children).ToList(),
                sumsChildren = sumsChildren
            };
        }

        public void SetOrder(int order)
        {
            MapItem.Order = order;
        }

        public IMapModel[] GetLeafModels()
        {
            if (cachedLeafModels != null)
                return cachedLeafModels;
            var v = new List<IMapModel>();
            AddLeafModels(v);

            cachedLeafModels = CopyArray(v);
            return cachedLeafModels;
        }

        private List<IMapModel> AddLeafModels(List<IMapModel> v)
        {
            if (!HasChildren)
                throw new InvalidOperationException("Somehow tried to get child model for leaf!!!");

            if (!GetChild(0).HasChildren)
                v.Add(this);
            else
                for (var i = ChildCount - 1; i >= 0; i--)
                    GetChild(i).AddLeafModels(v);
            return v;
        }

        public void Layout(IMapLayout tiling)
        {
            Layout(tiling, MapItem.Bounds);
        }

        public void Layout(IMapLayout tiling, Rect bounds)
        {
            MapItem.Bounds = bounds;
            if (!HasChildren) return;
            var s = Sum();
            tiling.Layout(this, bounds);
            for (var i = ChildCount - 1; i >= 0; i--)
                GetChild(i).Layout(tiling);
        }

        public IMappable[] GetTreeItems()
        {
            if (cachedTreeItems != null)
                return cachedTreeItems;

            var v = new List<IMappable>();
            AddTreeItems(v);

            cachedTreeItems = CopyArray(v);
            return cachedTreeItems;
        }

        private void AddTreeItems(List<IMappable> v)
        {
            if (!HasChildren)
                v.Add(MapItem);
            else
                for (var i = ChildCount - 1; i >= 0; i--)
                    GetChild(i).AddTreeItems(v);
        }

        private double Sum()
        {
            if (!sumsChildren)
                return MapItem.Size;
            double s = 0;
            for (var i = ChildCount - 1; i >= 0; i--)
                s += GetChild(i).Sum();
            MapItem.Size = s;
            return s;
        }

        public IMappable[] GetItems()
        {
            if (childItems != null)
                return childItems;
            var n = ChildCount;
            childItems = new IMappable[n];
            for (var i = 0; i < n; i++)
            {
                childItems[i] = GetChild(i).MapItem;
                childItems[i].Depth = 1 + Depth;
            }

            return childItems;
        }

        public void AddChild(TreeModel child)
        {
            child.Parent = this;
            children.Add(child);
            childItems = null;
        }
        
        public TreeModel GetChild(int n)
        {
            return (TreeModel) children[n];
        }

        private static IMapModel[] CopyArray(List<IMapModel> source)
        {
            return CopyArray(source.ToArray());
        }

        private static IMapModel[] CopyArray(IMapModel[] source)
        {
            var dest = new List<IMapModel>();

            dest.AddRange(source);
            return dest.ToArray();
        }

        private static IMappable[] CopyArray(List<IMappable> source)
        {
            return CopyArray(source.ToArray());
        }

        private static IMappable[] CopyArray(IMappable[] source)
        {
            var dest = new List<IMappable>();

            dest.AddRange(source);
            return dest.ToArray();
        }

        public override string ToString()
        {
            return $"{MapItem} with {ChildCount} children";
        }
        
    }
}