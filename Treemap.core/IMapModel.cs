﻿namespace Treemap.Core
{
    /// <summary>
    /// Model object used by MapLayout to represent data for a treemap.
    /// </summary>
    public interface IMapModel
    {
        /// <summary>
        /// Get the list of items in this model.
        /// </summary>
        /// <returns>An array of the Mappable objects in this MapModel.</returns>
        IMappable[] GetItems();

        IMapModel CopyTo();
    }
}