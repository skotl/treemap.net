﻿using System.Collections.Generic;
using Treemap.Core;

namespace Treemap.Example.Model
{
    public interface IDiscoveredEntry : IMappable
    {
        public string Path { get; }
        
        public List<IDiscoveredEntry> Contents { get;  }
    }
}