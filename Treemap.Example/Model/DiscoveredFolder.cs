﻿using System.Collections.Generic;
using System.Linq;
using Treemap.Core;

namespace Treemap.Example.Model
{
    public class DiscoveredFolder : IDiscoveredEntry
    {
        public string Path { get; }
        public DiscoveredFolder Parent { get; }
        public List<DiscoveredFolder> Directories { get; } = new List<DiscoveredFolder>();
        public List<DiscoveredFile> Files { get; } = new List<DiscoveredFile>();
        public IMappable CopyTo()
        {
            throw new System.NotImplementedException();
        }

        public double Size { get; set; }
        public Rect Bounds { get; set; }
        public int Order { get; set; }
        public int Depth { get; set; }

        public List<IDiscoveredEntry> Contents
        {
            get
            {
                var contents = new List<IDiscoveredEntry>();
                contents.AddRange(Directories.Select(d => d as IDiscoveredEntry));
                contents.AddRange(Files.Select(f => f as IDiscoveredEntry));

                return contents;
            }
        } 
            
        public DiscoveredFolder(string folder, DiscoveredFolder parentFolder)
        {
            Parent = parentFolder;
            Path = System.IO.Path.GetFullPath(folder);
            Depth = parentFolder?.Depth + 1 ?? 0;
        }

        public override string ToString()
        {
            return $"[{Depth}] {Path}, {Directories.Count} dirs, {Files.Count} files, {Size}B";
        }
    }
}