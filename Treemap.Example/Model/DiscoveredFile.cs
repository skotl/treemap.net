﻿using System.Collections.Generic;
using System.IO;
using Treemap.Core;

namespace Treemap.Example.Model
{
    public class DiscoveredFile : IDiscoveredEntry
    {
        public string Path { get; }
        public List<IDiscoveredEntry> Contents => null;

        public IMappable CopyTo()
        {
            throw new System.NotImplementedException();
        }
        
        public Rect Bounds { get; set; }
        public int Order { get; set; }
        public int Depth { get; set; }
        public double Size { get; set; }

        public DiscoveredFile(string path)
        {
            var fileInfo = new FileInfo(path);
            Path = fileInfo.FullName;
            Size = fileInfo.Length;
        }

        public override string ToString()
        {
            return $"{Path}, {Size}B";
        }
    }
}