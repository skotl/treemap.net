﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Treemap.Example.Model;

namespace Treemap.Example
{
    /// <summary>
    /// Populates a folder structure
    /// </summary>
    public class DirectoryGetter
    {
        /// <summary>
        /// Total size of this folder and all sub-entries beneath it
        /// </summary>
        public double TotalSize { get; private set; }
        
        /// <summary>
        /// Number of files in the structure
        /// </summary>
        public int NumFiles { get; private set; }
        
        /// <summary>
        /// Number of folders in the structure
        /// </summary>
        public int NumFolders { get; private set; }
        
        /// <summary>
        /// Root of the folder structure
        /// </summary>
        public DiscoveredFolder Root { get; set; }

        /// <summary>
        /// List of every folder found within the structure
        /// </summary>
        public List<DiscoveredFolder> AllFolders { get; set; }
        
        /// <summary>
        /// Populate the structure, starting at the supplied folder
        /// </summary>
        /// <param name="startingFolder"></param>
        public void Populate(string startingFolder)
        {
            AllFolders = FetchFileStructure(startingFolder);

            PropagateSizes(AllFolders);
            
            Root = AllFolders[0];
        }

        /// <summary>
        /// Starting at the most extreme distance from root, accumulate the file sizes into their parent folder's size,
        /// and recurse up the structure so that all folders reflect the total size of their contents 
        /// </summary>
        /// <param name="allDirectories"></param>
        private static void PropagateSizes(IEnumerable<DiscoveredFolder> allDirectories)
        {
            foreach (var byLevel in allDirectories
                .GroupBy(g => g.Depth)
                .OrderByDescending(g => g.Key))
            {
                foreach (var directoryEntry in byLevel)
                {
                    if (directoryEntry.Parent != null)
                        directoryEntry.Parent.Size += directoryEntry.Size;
                }
            }
        }

        /// <summary>
        /// Iterate through the folder structure, building a list of all folders and their contents
        /// </summary>
        /// <param name="startingFolder"></param>
        /// <returns></returns>
        private List<DiscoveredFolder> FetchFileStructure(string startingFolder)
        {
            var allDirectories = new List<DiscoveredFolder>();
            
            // Stack approach rather than method based recursion helps avoid, ironically, exhausting the program stack 
            var toProcess = new Stack<DiscoveredFolder>();
            var rootFolder = new DiscoveredFolder(startingFolder, null);
            toProcess.Push(rootFolder);

            while (toProcess.Count > 0)
            {
                var directory = toProcess.Pop();
                NumFolders++;
                allDirectories.Add(directory);

                try
                {
                    foreach (var subFolder in Directory.EnumerateDirectories(directory.Path))
                        toProcess.Push(new DiscoveredFolder(subFolder, directory));

                    foreach (var filePath in Directory.EnumerateFiles(directory.Path))
                    {
                        NumFiles++;
                        var file = new DiscoveredFile(filePath);
                        directory.Files.Add(file);
                        directory.Size += file.Size;
                        TotalSize += file.Size;
                    }
                }
                catch (Exception)
                {
                    // This isn't pretty, but this is just a proof-of-concept. Throw away any unauthorised access etc.
                    // exceptions and continue on our merry way
                }

                directory.Parent?.Directories.Add(directory);
            }

            return allDirectories;
        }

    }
}