﻿using Treemap.Core;
using Treemap.Example.Model;

namespace Treemap.Example
{
    public class TreeMapBuilder
    {
        private readonly DiscoveredFolder _folderStructureRoot;
        
        /// <summary>
        /// The populated Tree Model - set after calling <see cref="Build"/>
        /// </summary>
        public TreeModel TreeModel { get; private set; }

        public TreeMapBuilder(DiscoveredFolder folderStructureRoot)
        {
            _folderStructureRoot = folderStructureRoot;
        }

        /// <summary>
        /// Populate <see cref="TreeModel"/> from the supplied folder structure
        /// </summary>
        public TreeModel Build()
        {
            TreeModel =  AddTree(_folderStructureRoot, 0);
            
            return TreeModel;
        }
        
        /// Recursively add treemodels to branches. This isn't a great technique for *massively* deep data sets
        /// as it's possible to exhaust the stack - might be worth looking into using a queue instead
        private static TreeModel AddTree(IDiscoveredEntry item, int depth)
        {
            item.Depth = depth;
            var tree = new TreeModel(item);

            item.Contents?.ForEach(c =>
            {
                if (c.Size > 0)    // Important as the treemap algorithm can't handle zero-sized containers
                    tree.AddChild(
                        AddTree(c, depth + 1)
                    );
            });

            return tree;
        }
    }
}