﻿using System;
using Treemap.Core;
using Treemap.Example.HtmlOutput;
using Treemap.Example.Model;

namespace Treemap.Example
{
    class Program
    {
        /// <summary>
        /// Args are:
        ///     starting folder to recurse through
        ///     path to output HTML file
        ///
        /// Builds a list of all folders and files under the starting folder, builds a squarified tree map of the
        /// results, based on file size, then creates an HTML file that shows the tree map.
        /// Hovering over a square shows the file's path and size.
        ///
        /// This is not intended to be a finished / particularly usable solution, but is intended to demonstrate the
        /// tree map in action/ 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var parameters = new ProgramParameters(args);
            parameters.Get();
            
            Console.WriteLine($"Getting files from {parameters.BaseFolder}");
            var folderStruct = new DirectoryGetter();
            folderStruct.Populate(parameters.BaseFolder);

            Console.WriteLine("Building tree map");
            var treeMapBuilder = new TreeMapBuilder(folderStruct.Root);
            var tree = treeMapBuilder.Build();
            
            var bounds = new Rect(0, 0, 800, 600);
            var layout = new SquarifiedLayout();

            Console.WriteLine("Laying out tree map");
            tree.Layout(layout, bounds);

            Console.WriteLine("Creating output file");
            var htmlFileCreator = new TreeModelToHtmlFile(tree, parameters.OutputFile);
            htmlFileCreator.Create(bounds.w, bounds.h);
            
            Console.WriteLine("Done");
        }
    }
}