﻿using System;
using System.IO;

namespace Treemap.Example
{
    /// <summary>
    /// Break the supplied arguments into program parameters
    /// </summary>
    public class ProgramParameters
    {
        private readonly string[] _args;

        /// <summary>
        /// Base folder to search from
        /// </summary>
        public string BaseFolder { get; private set; }
        
        /// <summary>
        /// Path to the generated HTML output file
        /// </summary>
        public string OutputFile { get; private set; }

        public ProgramParameters(string[] args)
        {
            _args = args ?? throw new ArgumentNullException(nameof(args));
        }

        /// <summary>
        /// Processes the supplied arguments and generates program parameters.
        /// <para>Throws if invalid / insufficient arguments are supplied</para>
        /// </summary>
        public void Get()
        {
            if (_args.Length != 2)
                ShowUsageAndThrow(new ArgumentException("Missing program argument(s)"));
            
            BaseFolder = _args[0];
            OutputFile = _args[1];

            if (!Directory.Exists(BaseFolder))
                ShowUsageAndThrow(new DirectoryNotFoundException(BaseFolder));
        }

        private void ShowUsageAndThrow(Exception ex)
        {
            Usage();

            throw ex;
        }

        private void Usage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("    Treemap.Example.exe <base-folder> <output-file>");
            Console.WriteLine("where:");
            Console.WriteLine("    base-folder is the directory to read from");
            Console.WriteLine("    output-file is the HTML file to be created with the tree map");
        }
    }
}