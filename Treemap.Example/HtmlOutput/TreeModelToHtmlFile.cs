﻿using System;
using Treemap.Core;
using Treemap.Example.Model;

namespace Treemap.Example.HtmlOutput
{
    public class TreeModelToHtmlFile
    {
        private readonly TreeModel _treeModel;
        private readonly string _outputFile;

        /// <summary>
        /// Takes a <see cref="TreeModel"/> that has had a layout applied, and outputs a tree map to an HTML file
        /// that can be viewed in a browser
        /// </summary>
        /// <param name="treeModel"></param>
        /// <param name="outputFile"></param>
        public TreeModelToHtmlFile(TreeModel treeModel, string outputFile)
        {
            _treeModel = treeModel ?? throw new ArgumentNullException(nameof(treeModel));
            _outputFile = outputFile;
        }

        /// <summary>
        /// Create the HTML file with a canvas of the supplied size
        /// </summary>
        public void Create(in double width, in double height)
        {
            var htmlFile = new CanvasDrawer(_outputFile, width, height);
            
            AddToFile(_treeModel, htmlFile);
            htmlFile.Close();
        }
        
        /// <summary>
        /// This method, and its overload, will probably not cope well with large depths due to recursion /
        /// potential stack overflows. But it's only here as an example...
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="htmlFile"></param>
        private static void AddToFile(TreeModel tree, CanvasDrawer htmlFile)
        {
            AddToFile(tree.MapItem, htmlFile);
            for (var i = 0; i < tree.ChildCount; i++)
                AddToFile(tree.GetChild(i), htmlFile);
        }

        private static void AddToFile(IMappable item, CanvasDrawer htmlFile)
        {
            var discoveredEntry = item as IDiscoveredEntry;
            var title = $"{discoveredEntry.Path} ({FriendlyFileSize(discoveredEntry.Size)})";
            
            htmlFile.Append(item.Bounds, title);
        }


        /// <summary>
        /// Convert a double byte count to a user-friendly B, KB, MB... etc size
        /// <para>Source: https://stackoverflow.com/questions/281640/how-do-i-get-a-human-readable-file-size-in-bytes-abbreviation-using-net</para>
        /// </summary>
        /// <param name="byteCount"></param>
        /// <returns></returns>
        private static string FriendlyFileSize(double byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount <= 0.1)
                return "0" + suf[0];
            var bytes = Math.Abs(byteCount);
            var place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            var num = Math.Round(bytes / Math.Pow(1024, place), 1);
            
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
    }
}