﻿using System;
using System.IO;
using Treemap.Core;

namespace Treemap.Example.HtmlOutput
{
        public class CanvasDrawer : IDisposable
    {
        private const string TemplateFile = "HtmlOutput/template.html";
        private const string BoundaryMarker = "*RECTANGLES*";
        private const string HeightMarker = "*HEIGHT*";
        private const string WidthMarker = "*WIDTH*";
        
        private readonly double _width;
        private readonly double _height;
        private readonly StreamWriter _output;
        private readonly string _header;
        private readonly string _footer;
        private bool _disposed;

        /// <summary>
        /// Creates an HTML file that draws a canvas containing the supplied squares / rectangles
        /// </summary>
        /// <param name="outputFile"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public CanvasDrawer(string outputFile, double width, double height)
        {
            _width = width;
            _height = height;
            _output = new StreamWriter(outputFile);

            (_header, _footer) = ReadTemplate(TemplateFile, BoundaryMarker);

            _header = _header
                .Replace(HeightMarker, _height.ToString())
                .Replace(WidthMarker, _width.ToString());
            
            Append(_header);
        }
        
        ~CanvasDrawer()
        {
            Close();
        }

        public void Dispose()
        {
            _output?.Dispose();
        }

        /// <summary>
        /// Add a rectangle and its title to the array
        /// </summary>
        private void Append(double x, double y, double w, double h, string title)
        {
            title = title.Replace('\'', '"').Replace('\\', '/');
            Append($@"rects.push([{x}, {y}, {w}, {h}, '{title}']);");
        }

        /// <summary>
        /// Add a rectangle and its title to the array
        /// </summary>
        public void Append(Rect bounds, string title)
        {
            Append(bounds.x, bounds.y, bounds.w, bounds.h, title);
        }
        
        private void Append(string s)
        {
            _output.WriteLine(s);
        }
        
        /// <summary>
        /// Write the footer to the output file and close it
        /// </summary>
        public void Close()
        {
            if (_disposed)
                return;

            _disposed = true;
            Append(_footer);
            _output.Close();
            _output.Dispose();
        }

        /// <summary>
        /// Read the template HTML file and return a header and footer, where the template is split
        /// based on the location of the <see cref="BoundaryMarker"/> string.
        /// </summary>
        /// <param name="templateFile"></param>
        /// <param name="boundaryMarker"></param>
        /// <returns></returns>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="FormatException"></exception>
        private (string header, string footer) ReadTemplate(string templateFile, string boundaryMarker)
        {
            if (!File.Exists(templateFile))
                throw new FileNotFoundException(templateFile);

            var html = File.ReadAllText(templateFile);
            var marker = html.IndexOf(boundaryMarker);
            if (marker < 0)
                throw new FormatException($"Couldn't find the boundary marker '{boundaryMarker}' in {templateFile}");

            return (html.Substring(0, marker), html.Substring(marker + boundaryMarker.Length));
        }

    }
}