﻿namespace Treemap.Tests.IntegrationTests
{
    public class TestingDataBuilder
    {
        /// <summary>
        /// Returns a <see cref="TestingMapItem"/> with a structure of children beneath, reading to be
        /// turned into a <see cref="Treemap.Core.TreeModel"/>
        /// <para>The root item has a size that reflects the sum of all children, and each branch also
        /// has a size that reflects the sum of its leaves</para>
        /// </summary>
        /// <returns></returns>
        public TestingMapItem Build()
        {
            var rootItem = new TestingMapItem("root");
            rootItem.Size = 100;

            var itemA = BuildItem("a", 20, rootItem);
            var itemB = BuildItem("b", 50, rootItem);
            var itemC = BuildItem("c", 30, rootItem);

            var itemA1 = BuildItem("a1", 15, itemA);
            var itemA2 = BuildItem("a2", 5, itemA);

            var itemC1 = BuildItem("c1", 12, itemC);
            var itemC2 = BuildItem("c2", 6, itemC);
            var itemC3 = BuildItem("c3", 8, itemC);
            var itemC4 = BuildItem("c4", 4, itemC);

            var itemC3a = BuildItem("c3a", 5, itemC3);
            var itemC3b = BuildItem("c3b", 3, itemC3);

            return rootItem;
        }

        private TestingMapItem BuildItem(string name, int size, TestingMapItem parent)
        {
            var item = new TestingMapItem(name);
            item.Size = size;
            
            parent?.AddChild(item);
            return item;
        }

    }
}