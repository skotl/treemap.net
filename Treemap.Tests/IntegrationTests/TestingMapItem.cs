﻿using System.Collections.Generic;
using Treemap.Core;

namespace Treemap.Tests.IntegrationTests
{
    public class TestingMapItem : MapItem
    {
        /// <summary>
        /// Name of item, so we can easily check references
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// List of children, used to construct the tree map
        /// </summary>
        public List<TestingMapItem> Children { get;  } = new List<TestingMapItem>();
        
        public TestingMapItem()
        {}

        public TestingMapItem(string name)
        {
            Name = name;
        }

        public void AddChild(TestingMapItem child)
        {
            Children.Add(child);
        }

        public override string ToString()
        {
            return $"{Name} - {base.ToString()}";
        }

    }
}