﻿using System;
using System.Linq;
using Treemap.Core;
using Xunit;

namespace Treemap.Tests.IntegrationTests
{
    public static class SquarifiedLayoutTests
    {
        private const int BoundsWidth = 800;
        private const int BoundsHeight = 600;

        private static TreeModel BuildTreeModel()
        {
            var data = new TestingDataBuilder().Build();

            return AddTree(data, 0);
        }

        private static TreeModel AddTree(TestingMapItem item, int depth)
        {
            item.Depth = depth;
            var tree = new TreeModel(item);

            item.Children.ForEach(c =>
                tree.AddChild(
                    AddTree(c, depth + 1)
                ));

            return tree;
        }

        private static Rect GetBounds()
        {
            return new Rect(0, 0, BoundsWidth, BoundsHeight);
        }

        private static TreeModel ExecuteSquarifiedLayout()
        {
            var tree = BuildTreeModel();
            var bounds = GetBounds();
            var layout = new SquarifiedLayout();

            tree.Layout(layout, bounds);
            return tree;
        }

        [Fact]
        public static void CanSetRootItem()
        {
            var tree = ExecuteSquarifiedLayout();

            var rootItem = tree.MapItem;
            Assert.NotNull(rootItem);
            Assert.Equal(BoundsWidth, rootItem.Bounds.w);
            Assert.Equal(BoundsHeight, rootItem.Bounds.h);
            Assert.Equal(0, rootItem.Bounds.x);
            Assert.Equal(0, rootItem.Bounds.x);
        }

        [Fact]
        public static void RootItemHasThreeChildren()
        {
            var tree = ExecuteSquarifiedLayout();

            Assert.Equal(3, tree.ChildCount);
        }

        [Fact]
        public static void DescendentCountsAreCorrect()
        {
            var tree = ExecuteSquarifiedLayout();

            // a children, then b children, then c children
            Assert.Equal(2, tree.GetChild(0).ChildCount);
            Assert.Equal(0, tree.GetChild(1).ChildCount);
            Assert.Equal(4, tree.GetChild(2).ChildCount);

            // c3's children
            Assert.Equal(2, tree.GetChild(2).GetChild(2).ChildCount);
        }

        [Fact]
        public static void FamilyTreeHasExpectedItems()
        {
            var tree = ExecuteSquarifiedLayout();

            Assert.Equal("root", ((TestingMapItem) tree.MapItem).Name);

            Assert.Equal("a", ((TestingMapItem) tree.GetChild(0).MapItem).Name);
            Assert.Equal("b", ((TestingMapItem) tree.GetChild(1).MapItem).Name);
            Assert.Equal("c", ((TestingMapItem) tree.GetChild(2).MapItem).Name);

            Assert.Equal("a1", ((TestingMapItem) tree.GetChild(0).GetChild(0).MapItem).Name);
            Assert.Equal("a2", ((TestingMapItem) tree.GetChild(0).GetChild(1).MapItem).Name);

            Assert.Equal("c1", ((TestingMapItem) tree.GetChild(2).GetChild(0).MapItem).Name);
            Assert.Equal("c2", ((TestingMapItem) tree.GetChild(2).GetChild(1).MapItem).Name);
            Assert.Equal("c3", ((TestingMapItem) tree.GetChild(2).GetChild(2).MapItem).Name);
            Assert.Equal("c4", ((TestingMapItem) tree.GetChild(2).GetChild(3).MapItem).Name);

            Assert.Equal("c3a", ((TestingMapItem) tree.GetChild(2).GetChild(2).GetChild(0).MapItem).Name);
            Assert.Equal("c3b", ((TestingMapItem) tree.GetChild(2).GetChild(2).GetChild(1).MapItem).Name);
        }

        [Fact]
        public static void FamilyTreeHasExpectedParents()
        {
            var tree = ExecuteSquarifiedLayout();

            Assert.Null(tree.Parent);

            // a, b, c
            Assert.Equal(tree, tree.GetChild(0).Parent);
            Assert.Equal(tree, tree.GetChild(1).Parent);
            Assert.Equal(tree, tree.GetChild(2).Parent);

            Assert.Equal(GetExpectedNodeByName(tree, "a"), GetExpectedNodeByName(tree, "a1").Parent);
            Assert.Equal(GetExpectedNodeByName(tree, "a"), GetExpectedNodeByName(tree, "a2").Parent);
            
            Assert.Equal(GetExpectedNodeByName(tree, "c"), GetExpectedNodeByName(tree, "c1").Parent);
            Assert.Equal(GetExpectedNodeByName(tree, "c"), GetExpectedNodeByName(tree, "c2").Parent);
            Assert.Equal(GetExpectedNodeByName(tree, "c"), GetExpectedNodeByName(tree, "c3").Parent);
            Assert.Equal(GetExpectedNodeByName(tree, "c"), GetExpectedNodeByName(tree, "c4").Parent);

            Assert.Equal(GetExpectedNodeByName(tree, "c3"), GetExpectedNodeByName(tree, "c3a").Parent);
            Assert.Equal(GetExpectedNodeByName(tree, "c3"), GetExpectedNodeByName(tree, "c3b").Parent);
        }

        [Fact]
        public static void RootChildrenSizeSumEqualsParent()
        {
            var tree = ExecuteSquarifiedLayout();

            var totalSize = tree.MapItem.Size;
            Assert.Equal(100, totalSize);
            Assert.Equal(totalSize,
                tree.GetItems().Sum(e => e.Size)
            );
        }
        
        [Fact]
        public static void LevelAChildrenSizeSumEqualsParent()
        {
            var tree = ExecuteSquarifiedLayout();
            var node = GetExpectedNodeByName(tree, "a");
            
            var totalSize = node.MapItem.Size;
            Assert.Equal(20, totalSize);

            Assert.Equal(totalSize, node.GetItems().Sum(e => e.Size));
        }
        
        [Fact]
        public static void LevelBHasCorrectSizeAndNoChildren()
        {
            var tree = ExecuteSquarifiedLayout();
            var node = GetExpectedNodeByName(tree, "b");
            
            var totalSize = node.MapItem.Size;
            Assert.Equal(50, totalSize);
            Assert.Empty(node.GetItems());
        }
        
        [Fact]
        public static void LevelCChildrenSizeSumEqualsParent()
        {
            var tree = ExecuteSquarifiedLayout();
            var node = GetExpectedNodeByName(tree, "c");
            
            var totalSize = node.MapItem.Size;
            Assert.Equal(30, totalSize);

            Assert.Equal(totalSize, node.GetItems().Sum(e => e.Size));
        }
        
        [Fact]
        public static void LevelC3ChildrenSizeSumEqualsParent()
        {
            var tree = ExecuteSquarifiedLayout();
            var node = GetExpectedNodeByName(tree, "c3");
            
            var totalSize = node.MapItem.Size;
            Assert.Equal(8, totalSize);

            Assert.Equal(totalSize, node.GetItems().Sum(e => e.Size));
        }

        [Fact]
        public static void LayoutIsAsExpected()
        {
            var tree = ExecuteSquarifiedLayout();

            // root
            VerifyBounds(0, 0, 800, 600, tree.MapItem.Bounds);
            
            // a, b, c
            VerifyBounds(400, 360, 400, 240, GetExpectedNodeByName(tree, "a").MapItem.Bounds);
            VerifyBounds(0, 0, 400, 600, GetExpectedNodeByName(tree, "b").MapItem.Bounds);
            VerifyBounds(400, 0, 400, 360, GetExpectedNodeByName(tree, "c").MapItem.Bounds);
            
            // a1, a2
            VerifyBounds(400, 360, 300, 240, GetExpectedNodeByName(tree, "a1").MapItem.Bounds);
            VerifyBounds(700, 360, 100, 240, GetExpectedNodeByName(tree, "a2").MapItem.Bounds);
            
            // c1, c2, c3, c4
            VerifyBounds(400, 0, 320, 216, GetExpectedNodeByName(tree, "c1").MapItem.Bounds);
            VerifyBounds(720, 0, 80, 216, GetExpectedNodeByName(tree, "c2").MapItem.Bounds);
            VerifyBounds(400, 216, 320, 144, GetExpectedNodeByName(tree, "c3").MapItem.Bounds);
            VerifyBounds(720, 216, 80, 144, GetExpectedNodeByName(tree, "c4").MapItem.Bounds);
            
            // c3a, c3b
            VerifyBounds(400, 216, 200, 144, GetExpectedNodeByName(tree, "c3a").MapItem.Bounds);
            VerifyBounds(600, 216, 120, 144, GetExpectedNodeByName(tree, "c3b").MapItem.Bounds);
        }

        [Fact]
        public static void LayoutHasExpectedDepths()
        {
            var tree = ExecuteSquarifiedLayout();

            Assert.Equal(0, tree.Depth);
            
            Assert.Equal(1, GetExpectedNodeByName(tree, "a").Depth);
            Assert.Equal(1, GetExpectedNodeByName(tree, "b").Depth);
            Assert.Equal(1, GetExpectedNodeByName(tree, "c").Depth);
            
            Assert.Equal(2, GetExpectedNodeByName(tree, "a1").Depth);
            Assert.Equal(2, GetExpectedNodeByName(tree, "a2").Depth);
            Assert.Equal(2, GetExpectedNodeByName(tree, "c1").Depth);
            Assert.Equal(2, GetExpectedNodeByName(tree, "c2").Depth);
            Assert.Equal(2, GetExpectedNodeByName(tree, "c3").Depth);
            Assert.Equal(2, GetExpectedNodeByName(tree, "c4").Depth);
            
            Assert.Equal(3, GetExpectedNodeByName(tree, "c3a").Depth);
            Assert.Equal(3, GetExpectedNodeByName(tree, "c3b").Depth);
        }

        private static void VerifyBounds(double x, double y, double w, double h, Rect bounds)
        {
            Assert.True(AreCloseIsh(x, bounds.x), $"x: {x} != {bounds.x} ");
            Assert.True(AreCloseIsh(y, bounds.y), $"y: {y} != {bounds.y} ");
            Assert.True(AreCloseIsh(w, bounds.w), $"w: {w} != {bounds.w} ");
            Assert.True(AreCloseIsh(h, bounds.h), $"h: {h} != {bounds.h} ");
        }

        private static bool AreCloseIsh(double v1, double v2)
        {
            const double margin = 1.0d;

            return (Math.Abs(v1 - v2)) <= margin;
        }

        private static TreeModel GetExpectedNodeByName(TreeModel root, string name)
        {
            switch (name)
            {
                case "a": return root.GetChild(0);
                case "a1": return root.GetChild(0).GetChild(0);
                case "a2": return root.GetChild(0).GetChild(1);
                case "b": return root.GetChild(1);
                case "c": return root.GetChild(2);
                case "c1": return root.GetChild(2).GetChild(0);
                case "c2": return root.GetChild(2).GetChild(1);
                case "c3": return root.GetChild(2).GetChild(2);
                case "c3a": return root.GetChild(2).GetChild(2).GetChild(0);
                case "c3b": return root.GetChild(2).GetChild(2).GetChild(1);
                case "c4": return root.GetChild(2).GetChild(3);
                default: throw new ArgumentException(name);
            }
        }
    }
}