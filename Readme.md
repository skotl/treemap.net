# Treemap Algorithms .Net
This project is based on the Java sources published by the University of Maryland 
See http://www.cs.umd.edu/hcil/treemap/ for more info on the original projects.

# Compatibility
The project was built and tested using C# on dotnet core 3.1

# License
See [License.md](Treemap.core/License.md) for information on licensing and intellectual property rights.

# Nuget
A nuget package is available as [kwolo.treemap.net](https://www.nuget.org/packages/kwolo.treemap.net)

# Brief intro
For information on the algorithms themselves, see the original [UoM site](http://www.cs.umd.edu/hcil/treemap/).

An example project is supplied that creates a squarified treemap out of a folder structure,
showing relative file sizes beneath.

To run the sample, build `Treemap.Example` and run it as:

`Treemap.Example <base-folder> <html-file>`

The app will build a folder structure from `base-folder`, generate the tree map, with a squarified layout, then finally create an `html-file` that can be viewed in your browser.
It isn't intended to be a robust or complete application, but shows the steps involved in building a `TreeModel` structure and then executing a layout over it.

See the example's [Program.cs](Treemap.Example/Program.cs) to view the steps, and review the supplied classes to see how the structure is built.

